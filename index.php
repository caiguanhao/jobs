<?php
/* SHOWING MESSAGE */
$MESSAGE = '';
$cookie_crc32 = '';
if (isset($_COOKIE['CRC32'])) {
	$cookie_crc32 = $_COOKIE['CRC32'];
}
if (isset($_GET['crc32'])) {
	if ($cookie_crc32!=$_GET['crc32']) { // use cookie to prevent same msg showing again
		if (isset($_GET['okmsg'])) {
			$_GET['okmsg'] = base64_decode($_GET['okmsg']);
			if (crc32($_GET['okmsg'])==$_GET['crc32']) {
				$MESSAGE = "<div class=\"okmsg\">$_GET[okmsg]</div>";
				setcookie('CRC32', $_GET['crc32'], time()+3600*24*7, '/');
			}
		} else if (isset($_GET['errmsg'])) {
			$_GET['errmsg'] = base64_decode($_GET['errmsg']);
			if (crc32($_GET['errmsg'])==$_GET['crc32']) {
				$MESSAGE = "<div class=\"errmsg\">$_GET[errmsg]</div>";
				setcookie('CRC32', $_GET['crc32'], time()+3600*24*7, '/');
			}
		}
	} else {
		header('Location: /'); // reloading page with msg will redirect to homepage
	}
}
?>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link rel="stylesheet" href="/media/style/main.css" /> 
<script src="/media/scripts/jquery.js"></script>
<script src="/media/scripts/tinybox.js"></script>
<title>jobs</title>
</head>

<body>
<?php if ($MESSAGE!='') echo "\n$MESSAGE\n"; ?>

<h2>routine tasks</h2>

<?php
$crontabfile = "";
$tasklist = "";
$handle = popen('crontab -l', 'r');
$line = 0;
while (!feof($handle)) {
	$buffer = fgets($handle);
	$crontabfile .= $buffer;
	if (preg_match("/^([0-9\*\/,\-]+)\s([0-9\*\/,\-]+)\s([0-9\*\/,\-\?".
	"LW]+)\s([0-9\*\/,\-]+)\s([0-9\*\/,\-\?LW]+)\s(.*)$/", $buffer, $matches)) {
		$tasklist .= <<<_8HTML_
		<tr>
			<td>$matches[1]</td>
			<td>$matches[2]</td>
			<td>$matches[3]</td>
			<td>$matches[4]</td>
			<td>$matches[5]</td>
			<td>$matches[6]</td>
			<td>
				<a href="./?editroutinetask=$line">edit</a>
				<a class="needscfm" href="jobs.php?action=removeroutine&line=$line">remove</a>
			</td>
		</tr>

_8HTML_;
	} else if (strlen($buffer)>1) {
		if (substr($buffer, 0, 1)=='#') $add = ' style="display: none;"';
		$tasklist .= <<<_8HTML_
		<tr$add>
			<td class="oneline" colspan="6">$buffer</td>
			<td>
				<a href="./?editroutinetask=$line">edit</a>
				<a class="needscfm" href="jobs.php?action=removeroutine&line=$line">remove</a>
			</td>
		</tr>

_8HTML_;
	}
	$line += 1;
}
pclose($handle);

$scriptlist = "";
$scriptarray = array();
foreach (array_diff(scandir('scripts'), array('.', '..', 'index.php')) as $filename) {
	array_push($scriptarray, $filename);
	$filesize = number_format(filesize("scripts/$filename"));
	$fileatime = date('Y-m-d H:i:s', fileatime("scripts/$filename"));
	$filename2 = urlencode($filename);
	$scriptlist .= <<<_8HTML_
		<tr>
			<td><a class="scriptlnk" href="jobs.php?action=getscript&filename=$filename2">$filename</a></td>
			<td>$filesize bytes</td>
			<td>$fileatime</td>
			<td>
				<a href="./?editfile=$filename2#editscript">edit</a>
				<a class="needscfm" href="jobs.php?action=deletescript&filename=$filename2">delete</a>
			</td>
		</tr>

_8HTML_;
}

$joblist = "";
$handle = popen('atq', 'r');
while (!feof($handle)) {
	$buffer = fgets($handle);
	if (preg_match("/^(\d+)\t(.*)\s([A-Za-z=]{1})\s(.*)\n$/", $buffer, $matches)) {
		$matches[2] = strtotime($matches[2]);
		$matches[2] = date('Y-m-d H:i:s', $matches[2]);
		if ($matches[3] == '=') $matches[3] = 'running';
		$joblist .= <<<_8HTML_
		<tr>
			<td><a class="joblnk" href="jobs.php?action=getjobinfo&id=$matches[1]">$matches[1]</a></td>
			<td>$matches[2]</td>
			<td>$matches[3]</td>
			<td>$matches[4]</td>
			<td><a href="jobs.php?action=remove&id=$matches[1]">remove</a></td>
		</tr>

_8HTML_;
	}
}
pclose($handle);

if ($tasklist == "") {
	
echo <<<_8HTML_
<em>no routine tasks at the moment.</em>

_8HTML_;

} else {
	
echo <<<_8HTML_
<table class="tbljobs">
	<thead>
		<tr><td>min</td><td>hour</td><td>day</td><td>month</td><td>weekday</td><td>command</td><td>action</td></tr>
	</thead>
	<tbody>
$tasklist	</tbody>
	<tfoot>
		<td colspan="7"><a href="javascript:;" class="showhid">show hidden lines starting with '#'</a></td>
	</tfoot>
</table>

_8HTML_;

}
?>

<?php
$editroutinetask = null;
if (isset($_GET['editroutinetask']) && is_numeric($_GET['editroutinetask'])) {
	$editroutinetask = $_GET['editroutinetask'];
}
$usecustom = false;
if ($editroutinetask!=null && isset($crontabfile[$editroutinetask])) {
	$crontabfile = explode("\n", $crontabfile);
	if (!preg_match('/^([0-9\*\/,\-]+)\s([0-9\*\/,\-]+)\s([0-9\*\/,\-\?".
	"LW]+)\s([0-9\*\/,\-]+)\s([0-9\*\/,\-\?LW]+)\s(.*)$/',
	$crontabfile[$editroutinetask], $crontabinfo)) $usecustom = true;
} else $editroutinetask = null;
?>
<h2><?php echo $editroutinetask!=null?'edit a routine task':'add a routine task'; ?></h2>

<form action="jobs.php?action=addroutinetask<?php echo $editroutinetask!=null?'&line='.$editroutinetask:''; ?>" method="POST" id="frmrt" class="frmadd">
<input type="radio" name="rtm" value="0"<?php if (!$usecustom) echo ' checked'; ?> />
min:<input name="t1" size="4" value="<?php echo isset($crontabinfo[1])?$crontabinfo[1]:'*'; ?>" />
hr:<input name="t2" size="4" value="<?php echo isset($crontabinfo[2])?$crontabinfo[2]:'*'; ?>" />
d:<input name="t3" size="4" value="<?php echo isset($crontabinfo[3])?$crontabinfo[3]:'*'; ?>" />
m:<input name="t4" size="4" value="<?php echo isset($crontabinfo[4])?$crontabinfo[4]:'*'; ?>" />
wd:<input name="t5" size="4" value="<?php echo isset($crontabinfo[5])?$crontabinfo[5]:'*'; ?>" />
cmd:<input name="cmd" size="35" placeholder="" value="<?php echo str_replace('"', '&quot;', $crontabinfo[6]); ?>" /><br />
<input type="radio" name="rtm" value="1"<?php if ($usecustom) echo ' checked'; ?> />
custom:<input name="entry" size="70" id="custom" value="<?php if ($usecustom) echo str_replace('"', '&quot;', $crontabfile[$editroutinetask]); ?>" placeholder="* * * * *" /><br />
<input type="submit" value="<?php echo $editroutinetask!=null?'update':'add'; ?>" />
<?php if ($editroutinetask!=null) { ?><input type="submit" name="saveasnew" value="save as new" /><?php } ?>
<?php if ($editroutinetask!=null) { ?><input type="submit" name="cancel" value="cancel" /><?php } ?>
<select id="examples">
	<option value="">examples...</option>
	<option value="* * * * *">every minute</option>
	<option value="*/1 * * * *">every minute (2)</option>
	<option value="0-59 * * * *">every minute (3)</option>
	<option value="0 23 * * MON-FRI">23:00:00 every weekday night</option>
	<option value="2-59/3 1,9,22 11-26 1-6 *">11th to 26th of each month from Jan to Jun every 3rd min starting from 2 past 1am, 9am and 10pm</option>
	<option value="30 */2 *	* *">every 2 hours at :30</option>
	<option value="45 23 * * *">every day at 11:45PM</option>
	<option value="0 1 * * 0">every Sunday at 1:00AM</option>
	<option value="0 10,22 L * *">every last day of month at 10:00AM and 10:00PM</option>
	<option value="@yearly">once a year, midnight, Jan. 1st</option>
	<option value="@monthly">once a month, midnight, first of month</option>
	<option value="@weekly">once a week, midnight on Sunday</option>
	<option value="@daily">once a day, midnight</option>
	<option value="@hourly">once an hour, beginning of hour</option>
	<option value="@reboot">startup</option>
</select>
</form>

<?php
$t = microtime(true);
$d = new DateTime(date(sprintf("Y-m-d H:i:s.%06d",($t - floor($t)) * 1000000), $t));
$odate = array('Y', 'm', 'd', 'H', 'i', 's');
?>
<h2>list of jobs (<span id="curtime"><?php echo $d->format('Y-m-d H:i:s.') . substr($d->format('u'), 0, 3); ?></span>)</h2>

<script>
function pad(number) {
	var r=String(number);
	return r.length==1?'0'+r:r;
}
function pad2(number) {
	var r=String(number);
	return r.length==2?'0'+r:(r.length==1?'00'+r:r);
}
<?php
echo "var microsec=".substr($d->format('u'), 0, 3).";\n";
?>
var now, before = new Date();
setInterval(function(){
now = new Date();
microsec += now.getTime() - before.getTime();
<?php
$odate = array_map(create_function('$a', 'return $GLOBALS["d"]->format($a);'), $odate);
echo "var ndate = new Date(".implode(', ', $odate).", microsec);\n";
?>
$('#curtime').text(ndate.getFullYear() + '-' + pad(ndate.getMonth()) + '-' + pad(ndate.getDate()) + ' ' + pad(ndate.getHours()) + ':' + pad(ndate.getMinutes()) + ':' + pad(ndate.getSeconds()) + '.' + pad2(ndate.getMilliseconds()));
before = new Date();
}, 111);
</script>

<?php
if ($joblist == "") {
	
echo <<<_8HTML_
<em>no jobs at the moment.</em>

_8HTML_;

} else {
	
echo <<<_8HTML_
<table class="tbljobs">
	<thead>
		<tr><td>no.</td><td>time</td><td>queue</td><td>user</td><td>action</td></tr>
	</thead>
	<tbody>
$joblist	</tbody>
</table>

_8HTML_;

}
?>

<h2>add a job</h2>

<form action="jobs.php?action=add" method="POST" class="frmadd" id="addajob">
at -t <input name="time" title="time" value="<?php echo date('Y-m-d H:i', time()+60);?>" size="15" id="inptime" />
-f <select name="filename" title="script file" id="selfilename">
	<option value="">select...</option>
<?php
foreach ($scriptarray as $sfn) {
	$sfn2 = urlencode($sfn);
	echo <<<_8HTML_
		<option value="$sfn2">$sfn</option>

_8HTML_;
}
?>
</select>
-q <select name="queue" title="queue" id="selqueue">
<?php
foreach (array_merge(range('a', 'z'), range('A', 'Z')) as $letter) {
	echo <<<_8HTML_
	<option value="$letter">$letter</option>

_8HTML_;
}
?>
</select>
<input type="submit" value="add" />
<div class="cmdpreview">at -t <?php echo date('YmdHi', time()+60);?> -f "" -q a</div>
</form>

<h2>list of scripts</h2>

<?php
if ($scriptlist == "") {
	
echo <<<_8HTML_
<em>no scripts at the moment.</em>

_8HTML_;

} else {
	
echo <<<_8HTML_
<table class="tbljobs">
	<thead>
		<tr><td>file name</td><td>size</td><td>last access time</td><td>action</td></tr>
	</thead>
	<tbody>
$scriptlist	</tbody>
</table>

_8HTML_;

}
?>

<?php
$editfile = null;
if (isset($_GET['editfile']) && is_file('scripts/'.$_GET['editfile'])) {
	$editfile = $_GET['editfile'];
}
?>
<a name="editscript"></a>
<h2><?php echo $editfile?'edit a script':'add a script'; ?></h2>

<form action="jobs.php?action=addscript<?php echo $editfile?('&fromfile='.urlencode($editfile)):''; ?>" method="POST" class="frmadd">

scripts/<input id="inpnewname" name="filename" value="<?php echo $editfile?$editfile:date('\s\c\r\i\p\t_YmdHi.s'); ?>"<?php echo $editfile?' placeholder="'.$editfile.'"':''; ?> size="30" /><br />
<textarea name="scriptcontent" class="sarea" placeholder="# commands will be executed using /bin/sh"><?php
if ($editfile) echo @file_get_contents('scripts/'.$editfile);
?></textarea><br />
<input type="submit" value="<?php echo $editfile?'update':'add'; ?>" />
<?php if ($editfile) { ?><input id="btnsavenew" type="submit" name="saveasnew" value="save as new" /><?php } ?>
<?php if ($editfile) { ?><input type="submit" name="cancel" value="cancel" /><?php } ?>

</form>
<script>
$(function(){
	$('.errmsg, .okmsg').delay(5000).fadeOut('fast');
	$('.needscfm').bind('click', function(){
		if (!confirm('are you sure?')) return false;
	});
	$('.showhid').bind('click', function(){
		$('.tbljobs tbody tr:hidden').show();
		$(this).parents('tr').hide();
	});
	$('.joblnk, .scriptlnk').bind('click', function(){
		TINY.box.show({width: 700, height: 500, iframe: $(this).attr('href')});
		return false;
	});
	$('#btnsavenew').bind('click', function(){
		if ($('#inpnewname').val() == $('#inpnewname').attr('placeholder')) {
			alert('You must use a new file name.');
			$('#inpnewname').focus();
			return false;
		}
	});
	$('#inptime, #selfilename, #selqueue').bind('change', function(){
		$.post('jobs.php?action=add', $.merge($('#addajob'), $('<input />',{name:'preview', value:'true'})).serialize(), function(d){
			$('.cmdpreview').text($.parseJSON(d).output);
		});
	});
	$('#frmrt input:text').bind('focus', function(){
		$('input[type=radio][name=rtm][value=0]').prop('checked', !($(this).attr('id')=='custom'));
		$('input[type=radio][name=rtm][value=1]').prop('checked', $(this).attr('id')=='custom');
	});
	$('#examples').bind('change', function(){
		$('#custom').val($(this).val()).trigger('focus');
	});
});
</script>
</body>

</html>
