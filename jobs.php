<?php

$okmsg = '';
$errormsg = '';

if (isset($_GET['action'])) {
	$id = null;
	if (isset($_GET['id'])) {
		$id = $_GET['id'];
	}
	$filename = null;
	if (isset($_GET['filename'])) {
		$filename = 'scripts/' . $_GET['filename'];
	} else if (isset($_POST['filename'])) {
		$filename = 'scripts/' . $_POST['filename'];
	}
	$to_remove_routine = false;
	switch ($_GET['action']) {
		case 'removeroutine':
			$to_remove_routine = true;
		case 'addroutinetask':
			$saveasnew = false;
			if (isset($_POST['saveasnew'])) {
				$saveasnew = true;
			}
			if (isset($_POST['cancel'])) {
				header('Location: /');
				die;
			}
			$line = null;
			if (isset($_GET['line']) && is_numeric($_GET['line'])) {
				$line = $_GET['line'];
			}
			$usecustom = false;
			if (isset($_POST['rtm']) && $_POST['rtm']=='1') {
				$usecustom = true;
			}
			if ($usecustom) {
				$customcmd = null;
				if (isset($_POST['entry'])) {
					$customcmd = $_POST['entry'];
				}
				if ($customcmd == null) {
					$errormsg = "Invalid custom command parameter.";
					goto end;
				}
			}
			
			if (!$to_remove_routine && !$usecustom) {
				foreach (array('t1'=>'min', 't2'=>'hr', 't3'=>'d', 't4'=>'m', 't5'=>'wd') as $T => $K) {
					if (isset($_POST[$T]) && preg_match('/^[0-9\*\/,\-\?LW]+$/', $_POST[$T])) {
						$$T = $_POST[$T];
					} else {
						$errormsg = "Invalid $K parameter.";
						goto end;
					}
				}
				if (isset($_POST['cmd']) && strlen($_POST['cmd'])>0) {
					$cmd = $_POST['cmd'];
				} else {
					$errormsg = "Invalid cmd parameter.";
					goto end;
				}
			}
			
			$handle = popen('crontab -l', 'r');
			$crontabfile = array();
			while (!feof($handle)) {
				$buffer = str_replace("\n", '', fgets($handle));
				if ($buffer) array_push($crontabfile, $buffer);
			}
			pclose($handle);
			
			if ($to_remove_routine && $line!=null) {
				if (isset($crontabfile[$line])) {
					unset($crontabfile[$line]);
				} else {
					$errormsg = "Invalid line number.";
					goto end;
				}
			} else if (!$saveasnew && $line!=null) {
				if (isset($crontabfile[$line])) {
					if ($usecustom) {
						$crontabfile[$line] = $customcmd;
					} else {
						$crontabfile[$line] = sprintf('%s %s %s %s %s %s', $t1, $t2, $t3, $t4, $t5, $cmd);
					}
				} else {
					$errormsg = "Invalid line number.";
					goto end;
				}
			} else {
				if ($usecustom) {
					array_push($crontabfile, $customcmd);
				} else {
					array_push($crontabfile, sprintf('%s %s %s %s %s %s', $t1, $t2, $t3, $t4, $t5, $cmd));
				}
			}
			$crontabfile = implode("\n", $crontabfile);
			
			$crontabfile = rtrim($crontabfile, "\n");
			$crontabfile .= "\n"; // lastest version of crontab requires newline before EOF
			
			$handle = popen('crontab', 'w');
			fwrite($handle, $crontabfile);
			if (pclose($handle)==0) {
				$okmsg = "Routine tasks updated.";
			} else {
				$errormsg = "Error updating routine tasks.";
			}
			goto end;
			break;
		case 'deletescript':
			if ($filename && is_file($filename)) {
				if (unlink($filename)) {
					$okmsg = "File '{$filename}' deleted.";
					goto end;
				}
			}
			$errormsg = "Error deleting file '{$filename}'.";
			break;
		case 'addscript':
			if (isset($_POST['cancel'])) {
				header('Location: /');
				die;
			}
			$fromfile = null;
			if (isset($_GET['fromfile'])) {
				$fromfile = 'scripts/' . $_GET['fromfile'];
			}
			if ($filename == null) {
				$errormsg = "File name is required.";
				goto end;
			}
			$saveasnew = false;
			if (isset($_POST['saveasnew'])) {
				if ($filename == $fromfile) {
					$errormsg = "You must use a new file name.";
					goto end;
				} else {
					$saveasnew = true;
				}
			}
			$scriptcontent = null;
			if (isset($_POST['scriptcontent'])) {
				$scriptcontent = $_POST['scriptcontent'];
			}
			if ($scriptcontent == null) {
				$errormsg = "Script content is required.";
				goto end;
			}
			@touch($filename);
			if (is_writable($filename)) {
				if (file_put_contents($filename, $scriptcontent)!==FALSE && chmod($filename, 0644)) {
					if ($fromfile != $filename) {
						if (!$saveasnew) @unlink($fromfile);
					}
					if (!$fromfile) {
						$okmsg = "File '{$filename}' created.";
					} else {
						$okmsg = "File '{$filename}' updated.";
					}
					goto end;
				} else {
					$errormsg = "Error occurred when creating '{$filename}'.";
					goto end;
				}
			} else {
				$errormsg = "File '{$filename}' cannot be created or is not writable.";
				goto end;
			}
			break;
		case 'getscript':
			if ($filename && is_file($filename)) {
				echo '<html><head><meta http-equiv="Content-Type" content="text/html;charset=utf-8" /></head><body><pre>';
				echo htmlentities(file_get_contents($filename), ENT_COMPAT | ENT_HTML401, 'UTF-8');
				echo '</pre></body></html>';
				die;
			}
			break;
		case 'getjobinfo':
			if ($id && is_numeric($id)) {
				$handle = popen(sprintf('at -c %d', $id), 'r');
				echo '<html><head><meta http-equiv="Content-Type" content="text/html;charset=utf-8" /></head><body><pre>';
				while (!feof($handle)) {
					$buffer = fgets($handle);
					echo $buffer;
				}
				echo '</pre></body></html>';
				die;
			}
			break;
		case 'remove':
			if ($id && is_numeric($id)) {
				$handle = popen(sprintf('atrm %d', $id), 'r');
				if (pclose($handle)==0) {
					$okmsg = "Job removed.";
				} else {
					$errormsg = "Job cannot be removed.";
				}
				goto end;
			}
			break;
		case 'add':
			$preview = false;
			if (isset($_POST['preview'])) {
				if ($_POST['preview'] == 'true') {
					setcookie('CRC32', '', time()+3600*24*7, '/');
					$preview = true;
				}
			}
			$time = null;
			if (isset($_POST['time'])) {
				$time = $_POST['time'];
				$time = strtotime($time);
				$time = intval($time);
				if ($time <= time()) $time = null;
				if ($time != null) $time = date('YmdHi', $time);
			}
			if ($time == null) {
				$errormsg = "Invalid date/time. It must be in the future.";
				if ($preview) die(json_encode(array('output'=>$errormsg))); else goto end;
			}
			$queue = null;
			if (isset($_POST['queue'])) {
				$queue = $_POST['queue'];
				if (!preg_match('/^[a-zA-Z]{1}/', $queue)) {
					$errormsg = "Invalid queue parameter.";
					if ($preview) die(json_encode(array('output'=>$errormsg))); else goto end;
				}
			}
			$filename = urldecode($filename);
			$filename = realpath($filename);
			if ($filename && is_file($filename)) {
				$filename = addcslashes($filename, '"');
				$command = sprintf('at -t %s -f "%s" -q %.1s', $time, $filename, $queue?$queue:'a');
				if ($preview) {
					echo json_encode(array('output'=>$command));
					die;
				} else {
					$handle = popen($command, 'r');
					if (pclose($handle)==0) {
						$okmsg = "Job created.";
					} else {
						$errormsg = "Job cannot be created.";
					}
					goto end;
				}
			} else {
				$errormsg = "Input file does not exist or cannot be read.";
				if ($preview) die(json_encode(array('output'=>$errormsg))); else goto end;
			}
			break;
	}
}
end:
setcookie('CRC32', '', time()+3600*24*7, '/');
if ($okmsg!='') {
	header('Location: /?okmsg='.base64_encode($okmsg).'&crc32='.crc32($okmsg));
} else if ($errormsg!='') {
	header('Location: /?errmsg='.base64_encode($errormsg).'&crc32='.crc32($errormsg));
} else {
	header('Location: /');
}
